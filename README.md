# General Information

This repo contains GPL-licensed content from https://download.fsfe.org/campaigns/pmpc/share-general/src/pmpc-why.sv.svg with some adaptions (especially translation work).


# Original README

https://download.fsfe.org/campaigns/pmpc/share-general/src/


The vector graphic as well as the renders in Spanish and Galician were produced by Miguel A. Bouzada.

The script as well as additional information are available here: https://github.com/galpon/campa-aFSF-2017
